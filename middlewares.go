package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.uber.org/zap"
)

const loggerKey = "logger"

func initLoggingMiddleware(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {

		transactionIdKey := "Transaction-Id"
		transactionID := c.Request.Header.Get(transactionIdKey)
		if transactionID == "" {
			rndUUID, err := uuid.NewRandom()
			transactionID = rndUUID.String()
			if err != nil {
				logger.Error("Error while generating Transaction ID", zap.Error(err))
				transactionID = "8e77f8b1-da22-4f94-823e-2e3fa9da0b3d" // A default one to catch errors
			}
		}
		c.Writer.Header().Set(transactionIdKey, transactionID)

		requestLogger := logger.With(
			zap.String("path", c.Request.URL.Path),
			zap.String("transactionID", transactionID),
		)
		c.Set(loggerKey, requestLogger)

		c.Next()
	}
}

func loggingRequestMiddleware(c *gin.Context) {

	c.Next()

	logger := c.MustGet(loggerKey).(*zap.Logger)

	errors := make([]error, 0, len(c.Errors))
	for _, errorPtr := range c.Errors {
		errors = append(errors, errorPtr.Err)
	}

	loggerFunc := logger.Info
	if len(c.Errors.Errors()) > 0 {
		loggerFunc = logger.Error
	}

	loggerFunc("HTTP request",
		zap.Int("status", c.Writer.Status()),
		zap.Errors("errors", errors),
	)
}

func panicMiddleware(c *gin.Context) {

	c.Next()

	defer func() {
		if recoveredVal := recover(); recoveredVal != nil {

			logger := c.MustGet(loggerKey).(*zap.Logger)

			err := fmt.Errorf("%v", recoveredVal)
			logger.Error("Recovered from panic",
				zap.Error(err),
			)
			c.AbortWithError(http.StatusInternalServerError, err)
		}
	}()

}
