package breakglass_controls

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// Used for testing purposes, it should never be used in a real scenario
func exit(logger *zap.Logger) func(*gin.Context) {
	return func(c *gin.Context) {

		returnCode, err := strconv.Atoi(c.Param("returnCode"))
		if err != nil {
			returnedErr := fmt.Errorf("Parsing error of returnCode param: %w", err)
			c.AbortWithError(http.StatusBadRequest, returnedErr)
			return
		}

		logger.Info(
			"Manually restarting the component",
			zap.Int("returnCode", returnCode),
		)

		os.Exit(returnCode)
	}
}
