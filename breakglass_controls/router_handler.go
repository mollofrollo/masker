package breakglass_controls

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func RouterHandler(router *gin.Engine, logger *zap.Logger) {

	router.POST("/api/breakglass/exit/:returnCode", exit(logger))
}
