# Build
FROM golang:1.18-alpine AS build
RUN apk update
RUN apk add git
WORKDIR /app
COPY ./ ./
RUN go mod download
RUN go build -o masker

# Deploy
FROM alpine:latest
COPY --from=build /app/masker /opt/masker
ENV GIN_MODE=release
ENV PORT=8080
EXPOSE 8080
ENTRYPOINT ["/opt/masker"]
