package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/mollofrollo/masker/api_v1"
	"gitlab.com/mollofrollo/masker/breakglass_controls"
	"gitlab.com/mollofrollo/masker/monitoring"
	"go.uber.org/zap"
)

func main() {

	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("Log init error: %v", err)
	}
	defer logger.Sync()

	logger.Info("Masker started")

	router := gin.New()
	router.Use(panicMiddleware)
	router.Use(initLoggingMiddleware(logger))
	router.Use(loggingRequestMiddleware)

	api_v1.RouterHandler(router, logger)
	breakglass_controls.RouterHandler(router, logger)
	monitoring.RouterHandler(router, logger)

	logger.Fatal("HTTP server internal error", zap.Error(router.Run()))
}
