package monitoring

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func RouterHandler(router *gin.Engine, logger *zap.Logger) {

	router.GET("/api/monitoring/healthcheck", healthcheck(logger))
}
