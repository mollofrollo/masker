package monitoring

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func healthcheck(logger *zap.Logger) func(*gin.Context) {
	return func(c *gin.Context) {

		c.Status(http.StatusOK)
	}
}
