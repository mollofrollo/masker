package api_v1

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func mask(logger *zap.Logger) func(c *gin.Context) {
	return func(c *gin.Context) {

		pipelineID := c.Param("pipelineID")

		salt, err := getSalt(pipelineID)
		if err != nil {
			if errors.As(err, &pipelineNotFound{}) {
				c.AbortWithError(http.StatusNotFound, err)
			} else {
				c.AbortWithError(http.StatusBadRequest, err)
			}
			return
		}

		body, err := parseBody(c)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		maskedElements, err := maskElements(body.Elements, salt)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		c.JSON(http.StatusOK, responseBody{
			Elements: maskedElements,
		})

	}
}

type requestBody struct {
	Elements map[string]string `json:"elements" binding:"required"`
}

type responseBody = requestBody

func parseBody(c *gin.Context) (requestBody, error) {

	var request requestBody
	err := c.ShouldBindJSON(&request)
	return request, err

}
