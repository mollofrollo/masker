package api_v1

import (
	"bytes"
	"testing"
)

func FuzzMaskString__shouldNeverFail(f *testing.F) {

	f.Add("input-placeholder", make([]byte, 0)) // No salt
	f.Add("", []byte{0, 3, 5, 9})               // No input
	f.Add("", make([]byte, 0))                  // No input, no salt

	f.Fuzz(func(t *testing.T, input string, salt []byte) {
		hashedValue, err := maskString(input, salt)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}
		if hashedValue == "" {
			t.Errorf(
				"Empty string is not a valid hash of %s with salt %v",
				input, salt,
			)
		}
	})

}

func FuzzMaskString__differentSaltsGenerateDifferentHashes(f *testing.F) {

	f.Add("input-placeholder", []byte{0, 3, 5, 9}, make([]byte, 0))    // Hash with salt VS no salt
	f.Add("input-placeholder", []byte{0, 3, 5, 9}, []byte{0, 3, 5})    // Salt with different lenght
	f.Add("input-placeholder", []byte{0, 3, 5, 9}, []byte{0, 3, 5, 8}) // Different salt with the same lenght

	f.Fuzz(func(t *testing.T, input string, salt1 []byte, salt2 []byte) {
		if !bytes.Equal(salt1, salt2) {
			hashedValue1, _ := maskString(input, salt1)
			hashedValue2, _ := maskString(input, salt2)

			if hashedValue1 == hashedValue2 {
				t.Errorf(
					"Different salts should generate different hashes, but found %s with input %s, salt %v and salt %v",
					hashedValue1, input, salt1, salt2,
				)
			}
		}
	})

}

func FuzzMaskString__differentInputsGenerateDifferentHashes(f *testing.F) {

	f.Add("input-a", "input-b", make([]byte, 0))    // No salt
	f.Add("input-a", "input-b", []byte{0, 3, 5, 9}) // With salt

	f.Fuzz(func(t *testing.T, input1 string, input2 string, salt []byte) {
		if input1 != input2 {
			hashedValue1, _ := maskString(input1, salt)
			hashedValue2, _ := maskString(input2, salt)

			if hashedValue1 == hashedValue2 {
				t.Errorf(
					"Different inputs should generate different hashes, but found %s with salt %v, input %s and input %s",
					hashedValue1, salt, input1, input2,
				)
			}
		}
	})

}

func TestMaskElements(t *testing.T) {

	elements := map[string]string{
		"name":    "Davide",
		"surname": "Riva",
	}

	maskedElements, err := maskElements(elements, []byte{3, 8, 4})
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	if len(elements) != len(maskedElements) {
		t.Errorf(
			"Input lenght (%d) and output lenght (%d) should match",
			len(elements), len(maskedElements),
		)
	}

	for key, maskedElement := range maskedElements {
		element := elements[key]

		if element == maskedElement {
			t.Errorf(
				"Hashed value should not match with the original value, found: %s",
				maskedElement,
			)
		}
	}
}
