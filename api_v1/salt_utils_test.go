package api_v1

import (
	"bytes"
	"encoding/base64"
	"errors"

	"fmt"
	"os"
	"testing"
)

func TestGetSalt__existingEnvVar(t *testing.T) {

	pipelineID := " dummy_pipeline_name  "
	envVarName := fmt.Sprintf("%s%s", envVarNamePrefix, "DUMMY_PIPELINE_NAME")
	originalSalt := []byte("PLACEHOLDER")

	previousEnvVal := os.Getenv(envVarName)
	os.Setenv(envVarName, base64.StdEncoding.EncodeToString(originalSalt))
	retrivedSalt, err := getSalt(pipelineID)
	os.Setenv(envVarName, previousEnvVal)

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	if !bytes.Equal(originalSalt, retrivedSalt) {
		t.Errorf("%s expected, retrieved: %s", originalSalt, retrivedSalt)
	}

}

func TestGetSalt__nonexistentEnvVar(t *testing.T) {

	pipelineID := "dummy_pipeline_name"
	envVarName := fmt.Sprintf("%s%s", envVarNamePrefix, "DUMMY_PIPELINE_NAME")

	previousEnvVal := os.Getenv(envVarName)
	os.Unsetenv(envVarName)
	salt, err := getSalt(pipelineID)
	os.Setenv(envVarName, previousEnvVal)

	if err == nil {
		t.Errorf("Retrieving a nonexistent secret should fail, found: %v", salt)
	}

	if !errors.As(err, &pipelineNotFound{}) {
		t.Errorf("Retrieving an empty pipeline ID should fail, found: %v", salt)
	}
}

func TestGetSalt__emptyPipelineID(t *testing.T) {

	pipelineID := "     "
	envVarName := envVarNamePrefix

	previousEnvVal := os.Getenv(envVarName)
	os.Setenv(envVarName, "UExBQ0VIT0xERVI=")
	salt, err := getSalt(pipelineID)
	os.Setenv(envVarName, previousEnvVal)

	if err == nil {
		t.Errorf("Retrieving an empty pipeline ID should fail, found: %v", salt)
	}

	if !errors.As(err, &pipelineNotFound{}) {
		t.Errorf("Retrieving an empty pipeline ID should fail, found: %v", salt)
	}
}
