package api_v1

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func TestMask__validRequest(t *testing.T) {

	logger, err := zap.NewDevelopment()
	if err != nil {
		t.Fatalf("Error while init logger: %v", err)
	}

	pipelineID := "DUMMY_PIPELINE_NAME"
	envVarName := fmt.Sprintf("%s%s", envVarNamePrefix, pipelineID)
	url := fmt.Sprintf("/api/v1/mask/%s", pipelineID)

	originalKey := "name"
	originalValue := "Davide"
	request, err := json.Marshal(requestBody{
		Elements: map[string]string{
			originalKey: originalValue,
		},
	})
	if err != nil {
		t.Fatalf("Error while marshalling json: %v", err)
	}

	previousEnvVal := os.Getenv(envVarName)
	os.Setenv(envVarName, "UExBQ0VIT0xERVI=")

	router := gin.Default()
	RouterHandler(router, logger)

	w := httptest.NewRecorder()

	req, err := http.NewRequest("POST", url, bytes.NewReader(request))
	if err != nil {
		os.Setenv(envVarName, previousEnvVal)
		t.Fatalf("Error while building request: %v", err)
	}

	router.ServeHTTP(w, req)
	response := w.Result()

	os.Setenv(envVarName, previousEnvVal)

	if response.StatusCode != http.StatusOK {
		t.Fatalf("200 status expected, found: %d", response.StatusCode)
	}

	jsonResponse := responseBody{}
	err = json.NewDecoder(response.Body).Decode(&jsonResponse)
	if err != nil {
		t.Fatalf("Error while decoding the response: %v", err)
	}

	_, keyExists := jsonResponse.Elements[originalKey]
	if !keyExists {
		t.Fatalf("Original key %s not found in the response", originalKey)
	}

}

func TestMask__malformedRequest(t *testing.T) {

	logger, err := zap.NewDevelopment()
	if err != nil {
		t.Fatalf("Error while init logger: %v", err)
	}

	pipelineID := "DUMMY_PIPELINE_NAME"
	envVarName := fmt.Sprintf("%s%s", envVarNamePrefix, pipelineID)
	url := fmt.Sprintf("/api/v1/mask/%s", pipelineID)

	request := []byte(`{
		"malformed": "request"
	}`)

	previousEnvVal := os.Getenv(envVarName)
	os.Setenv(envVarName, "UExBQ0VIT0xERVI=")

	router := gin.Default()
	RouterHandler(router, logger)

	w := httptest.NewRecorder()

	req, err := http.NewRequest("POST", url, bytes.NewReader(request))
	if err != nil {
		os.Setenv(envVarName, previousEnvVal)
		t.Fatalf("Error while building request: %v", err)
	}

	router.ServeHTTP(w, req)
	response := w.Result()

	os.Setenv(envVarName, previousEnvVal)

	if response.StatusCode != http.StatusBadRequest {
		t.Fatalf("200 status expected, found: %d", response.StatusCode)
	}

}

func TestMask__nonexistentPipeline(t *testing.T) {

	logger, err := zap.NewDevelopment()
	if err != nil {
		t.Fatalf("Error while init logger: %v", err)
	}

	pipelineID := "DUMMY_PIPELINE_NAME"
	envVarName := fmt.Sprintf("%s%s", envVarNamePrefix, pipelineID)
	url := fmt.Sprintf("/api/v1/mask/%s", pipelineID)

	request, err := json.Marshal(requestBody{
		Elements: map[string]string{
			"name": "Davide",
		},
	})
	if err != nil {
		t.Fatalf("Error while marshalling json: %v", err)
	}

	previousEnvVal := os.Getenv(envVarName)
	os.Unsetenv(envVarName)

	router := gin.Default()
	RouterHandler(router, logger)

	w := httptest.NewRecorder()

	req, err := http.NewRequest("POST", url, bytes.NewReader(request))
	if err != nil {
		os.Setenv(envVarName, previousEnvVal)
		t.Fatalf("Error while building request: %v", err)
	}

	router.ServeHTTP(w, req)
	response := w.Result()

	os.Setenv(envVarName, previousEnvVal)

	if response.StatusCode != http.StatusNotFound {
		t.Fatalf("404 status expected, found: %d", response.StatusCode)
	}

}
