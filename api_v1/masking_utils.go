package api_v1

import (
	"encoding/base64"
	"fmt"

	"golang.org/x/crypto/sha3"
)

func maskElements(elements map[string]string, salt []byte) (map[string]string, error) {

	maskedElements := map[string]string{}

	for key, value := range elements {
		maskedValue, err := maskString(value, salt)
		if err != nil {
			return maskedElements, fmt.Errorf("error while hashing key %s: %w", key, err)
		}
		maskedElements[key] = maskedValue
	}

	return maskedElements, nil
}

func maskString(input string, salt []byte) (string, error) {

	hasher := sha3.New512()
	if _, err := hasher.Write(salt); err != nil {
		return "", fmt.Errorf("error while using the salt %v during hashing: %w", salt, err)
	}
	if _, err := hasher.Write([]byte(input)); err != nil {
		return "", fmt.Errorf("error while using the input %s during hasing: %w", input, err)
	}

	return base64.StdEncoding.EncodeToString(hasher.Sum(nil)), nil
}
