package api_v1

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func RouterHandler(router *gin.Engine, logger *zap.Logger) {

	router.POST("/api/v1/mask/:pipelineID", mask(logger))
}
