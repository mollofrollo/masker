package api_v1

import (
	"encoding/base64"
	"fmt"
	"os"
	"strings"
)

type pipelineNotFound struct {
	pipelineID string
}

func (m pipelineNotFound) Error() string {

	return fmt.Sprintf("pipeline ID not found: %s", m.pipelineID)
}

const envVarNamePrefix = "SALT_"

func getSalt(pipelineID string) ([]byte, error) {

	formattedPipeline := strings.ToUpper(
		strings.TrimSpace(pipelineID),
	)
	if len(formattedPipeline) == 0 {
		return nil, pipelineNotFound{pipelineID: pipelineID}
	}

	envVarName := fmt.Sprintf("%s%s", envVarNamePrefix, formattedPipeline)

	salt, envVarFound := os.LookupEnv(envVarName)
	if !envVarFound {
		return nil, pipelineNotFound{pipelineID: pipelineID}
	}

	return base64.StdEncoding.DecodeString(salt)
}
